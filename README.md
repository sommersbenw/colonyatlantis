# Ben Sommers 2nd Project #

This project was started early-mid 2017, with a desire to start with fresh code.
I used a [series of tutorials](http://catlikecoding.com/unity/tutorials/hex-map/part-1/) that focus on a hex-grid map setup for turn based strategy to learn more on the graphics display side of coding in Unity. 
The codebase is in the /Assets/Scripts/ directory, and the code is a mix of outside code and my own creations and modifications. Towards the end of this project's timeline, I spent more time exploring the graphics side and creating new 3D Objects to display over coding new features.
This project was put on hold as my VUMC Project was ramping up >50 hours a week in time committment, due to lack of time for proper focus. 