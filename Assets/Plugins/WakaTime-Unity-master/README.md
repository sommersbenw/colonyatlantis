# WakaTime-Unity

WakaTime integration in the Unity editor. Enables you to track the time you spend working in the Unity Editor on cool games.

![](https://i.imgur.com/ygGLNUw.png)

## Installation

- Copy plugin into your `Assets` directory
- Open Unity and navigate to your Preferences.
- Open up the shiny new `WakaTime` tab and input your API key.

The preferences window is a little buggy at the moment, but after you submit your API key and it gets validated it should auto-detect your project name. If it doesn't and the dropdown doesn't populate, close the preferences window and re-open it. Will be fixed soon.

**Only tested on Mac. Highly appreciate someone to validate it works on Windows!**