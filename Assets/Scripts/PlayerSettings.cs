﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlayerSettings : ScriptableObject {

    [Header("Debug")]
    public bool debugMode;

    [Header("Units")]
    public HexUnit[] hexUnits;

    [Header("Buildings")]
    public HexBuilding[] hexBuildings;

    [Header("Materials")]
    public Material primaryTeamColor;
    public Material secondaryTeamColor;

}
