﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{

    private int supplies_, playerNumber_;
    public PlayerSettings ps;
    

    public int Supplies //getter/setter
    {
        get { return supplies_; }

        set {
            supplies_ = value;
        }
    }

    public int PlayerNumber //getter/setter
    {
        get { return playerNumber_; }

        set {
            playerNumber_ = value;
        }
    }

    public Player(int startingSupplies, int playerNumber, PlayerSettings playerSettings) {
        supplies_ = startingSupplies;
        playerNumber_ = playerNumber;
        //TODO this needs to be more dynamic moving forward
        if (playerNumber == 1)
        {
            ps = playerSettings;
        } else
        {
            ps = playerSettings;
        }
           
    }
}
