﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class HexMapEditor : MonoBehaviour {


    public HexGrid hexGrid;
    public Player selectedPlayer;
    public Material terrainMaterial;
    public GameObject leftMenu, rightMenu, editMenu;

	int activeElevation;
	int activeWaterLevel;

	int activeUrbanLevel, activeRockLevel, activePlantLevel, activeTreeLevel, activeSpecialIndex;

	int activeTerrainTypeIndex = -1;

	int brushSize;

	bool applyElevation = false;
	bool applyWaterLevel = false;

	bool applyUrbanLevel, applyRockLevel, applyPlantLevel, applyTreeLevel, applySpecialIndex;
    bool buildFactoryBuilding;

	enum OptionalToggle {
		Ignore, Yes, No
	}

	OptionalToggle riverMode, roadMode, walledMode;

	bool isDrag;
	HexDirection dragDirection;
	HexCell previousCell;

	public void SetTerrainTypeIndex (int index) {
		activeTerrainTypeIndex = index;
	}

	public void SetApplyElevation (bool toggle) {
		applyElevation = toggle;
	}

	public void SetElevation (float elevation) {
		activeElevation = (int)elevation;
	}

	public void SetApplyWaterLevel (bool toggle) {
		applyWaterLevel = toggle;
	}

	public void SetWaterLevel (float level) {
		activeWaterLevel = (int)level;
	}

	public void SetApplyUrbanLevel (bool toggle) {
		applyUrbanLevel = toggle;
	}

	public void SetUrbanLevel (float level) {
		activeUrbanLevel = (int)level;
	}

	public void SetApplyRockLevel (bool toggle) {
		applyRockLevel = toggle;
	}

	public void SetRockLevel (float level) {
		activeRockLevel = (int)level;
	}

	public void SetApplyPlantLevel (bool toggle) {
		applyPlantLevel = toggle;
	}

	public void SetPlantLevel (float level) {
		activePlantLevel = (int)level;
	}

    public void SetApplyTreeLevel(bool toggle)
    {
        applyTreeLevel = toggle;
    }

    public void SetTreeLevel(float level)
    {
        activeTreeLevel = (int)level;
    }

    public void SetApplySpecialIndex (bool toggle) {
		applySpecialIndex = toggle;
	}

	public void SetSpecialIndex (float index) {
		activeSpecialIndex = (int)index;
	}

	public void SetBrushSize (float size) {
		brushSize = (int)size;
	}

	public void SetRiverMode (int mode) {
		riverMode = (OptionalToggle)mode;
	}

	public void SetRoadMode (int mode) {
		roadMode = (OptionalToggle)mode;
	}

	public void SetWalledMode (int mode) {
		walledMode = (OptionalToggle)mode;
	}

    public void SetFactoryCreate(bool toggle)
    {
        buildFactoryBuilding = toggle;
    }

    public void ChangeSelectedPlayer(int playerIndex)
    {
        selectedPlayer = hexGrid.playerList[playerIndex];
    }

    public void EnableEditUI() {
        enabled = true;
        rightMenu.SetActive(true);
        leftMenu.SetActive(true);
        editMenu.SetActive(false);
    }

    public void EnableGameUI() {
        enabled = false;
        rightMenu.SetActive(false);
        leftMenu.SetActive(false);
        editMenu.SetActive(true);
    }

	public void ShowGrid (bool visible) {
		if (visible) {
			terrainMaterial.EnableKeyword("GRID_ON");
		}
		else {
			terrainMaterial.DisableKeyword("GRID_ON");
		}
	}

	void Awake () {
		terrainMaterial.DisableKeyword("GRID_ON");
        EnableEditUI();
        selectedPlayer = hexGrid.playerList[0];
    }

	void Update () {
		if (!EventSystem.current.IsPointerOverGameObject()) {
			if (Input.GetMouseButton(0)) {
				HandleInput();
				return;
			}
			if (Input.GetKeyDown(KeyCode.U)) {
				if (Input.GetKey(KeyCode.LeftShift)) {
					DestroyUnit();
				}
				else {
					CreateUnit();
				}
				return;
			}
            /*if (Input.GetKeyDown(KeyCode.B)) {
                if (Input.GetKey(KeyCode.LeftShift)) {
                    DestroyBuilding();
                } else {
                    CreateBuilding();
                }
                return;
            }*/
        }
		previousCell = null;
	}

	HexCell GetCellUnderCursor () {
		return
			hexGrid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
	}

	void CreateUnit () {
		HexCell cell = GetCellUnderCursor();
		if (cell && !cell.Unit) {
            HexUnit unitToAdd = Array.Find<HexUnit>(selectedPlayer.ps.hexUnits, t => t.name.Contains("Tank"));
			hexGrid.AddUnit(
				Instantiate(unitToAdd), cell, UnityEngine.Random.Range(0f, 360f), 1
            );
		}
	}

	void DestroyUnit () {
		HexCell cell = GetCellUnderCursor();
		if (cell && cell.Unit) {
			hexGrid.RemoveUnit(cell.Unit);
		}
	}

    void CreateBuilding() {
        HexCell cell = GetCellUnderCursor();
        if (cell && !cell.Building) {
            HexBuilding buildingToAdd = Array.Find<HexBuilding>(selectedPlayer.ps.hexBuildings, t => t.name.Contains("Factory"));
            hexGrid.AddBuilding(
                Instantiate(buildingToAdd), cell, UnityEngine.Random.Range(0f, 360f), selectedPlayer.PlayerNumber
            );
        }
    }

    void DestroyBuilding() {
        HexCell cell = GetCellUnderCursor();
        if (cell && cell.Building) {
            hexGrid.RemoveBuilding(cell.Building);
        }
    }

    void HandleInput () {
		HexCell currentCell = GetCellUnderCursor();
		if (currentCell) {
			if (previousCell && previousCell != currentCell) {
				ValidateDrag(currentCell);
			}
			else {
				isDrag = false;
			}
			EditCells(currentCell);
			previousCell = currentCell;
		}
		else {
			previousCell = null;
		}
	}

	void ValidateDrag (HexCell currentCell) {
		for (
			dragDirection = HexDirection.NE;
			dragDirection <= HexDirection.NW;
			dragDirection++
		) {
			if (previousCell.GetNeighbor(dragDirection) == currentCell) {
				isDrag = true;
				return;
			}
		}
		isDrag = false;
	}

	void EditCells (HexCell center) {
		int centerX = center.coordinates.X;
		int centerZ = center.coordinates.Z;

		for (int r = 0, z = centerZ - brushSize; z <= centerZ; z++, r++) {
			for (int x = centerX - r; x <= centerX + brushSize; x++) {
				EditCell(hexGrid.GetCell(new HexCoordinates(x, z)));
			}
		}
		for (int r = 0, z = centerZ + brushSize; z > centerZ; z--, r++) {
			for (int x = centerX - brushSize; x <= centerX + r; x++) {
				EditCell(hexGrid.GetCell(new HexCoordinates(x, z)));
			}
		}
	}

	void EditCell (HexCell cell) {
		if (cell) {
			if (activeTerrainTypeIndex >= 0) {
				cell.TerrainTypeIndex = activeTerrainTypeIndex;
			}
			if (applyElevation) {
				cell.Elevation = activeElevation;
			}
			if (applyWaterLevel) {
				cell.WaterLevel = activeWaterLevel;
			}
			if (applySpecialIndex) {
				cell.SpecialIndex = activeSpecialIndex;
			}
			if (applyUrbanLevel) {
				cell.UrbanLevel = activeUrbanLevel;
			}
			if (applyRockLevel) {
				cell.RockLevel = activeRockLevel;
			}
			if (applyPlantLevel) {
				cell.PlantLevel = activePlantLevel;
			}
            if (applyTreeLevel){
                cell.TreeLevel = activeTreeLevel;
            }
            if (riverMode == OptionalToggle.No) {
				cell.RemoveRiver();
			}
			if (roadMode == OptionalToggle.No) {
				cell.RemoveRoads();
			}
			if (walledMode != OptionalToggle.Ignore) {
				cell.Walled = walledMode == OptionalToggle.Yes;
			}

            if (buildFactoryBuilding){
                CreateBuilding();
            }

			if (isDrag) {
				HexCell otherCell = cell.GetNeighbor(dragDirection.Opposite());
				if (otherCell) {
					if (riverMode == OptionalToggle.Yes) {
						otherCell.SetOutgoingRiver(dragDirection);
					}
					if (roadMode == OptionalToggle.Yes) {
						otherCell.AddRoad(dragDirection);
					}
				}
			}
		}
	}
}