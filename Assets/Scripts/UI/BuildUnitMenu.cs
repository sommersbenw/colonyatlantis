﻿using UnityEngine;
using System;

public class BuildUnitMenu : MonoBehaviour {

    public HexGameUI hexGameUI;
    public HexGrid hexGrid;
    private HexCell buildingCell;

    public void Open() {
        buildingCell = GetCellUnderCursor();
        gameObject.SetActive(true);
        HexMapCamera.Locked = true;
    }

    public void Close() {
        gameObject.SetActive(false);
        HexMapCamera.Locked = false;
    }

    HexCell GetCellUnderCursor()
    {
        return
            hexGrid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
    }

    public void CreateInfantry() {
        CreateUnit("Infantry");
    }

    public void CreateCruiser() {
        CreateUnit("Cruiser");
    }

    public void CreateAircraft() {
        CreateUnit("Aircraft");
    }

    public void CreateTank() {
        CreateUnit("Tank");
    }

    void CreateUnit(string unitType)
    {
        if (buildingCell && !buildingCell.Unit)
        {
            hexGameUI.AddUnitToGrid(unitType, buildingCell);
            Close();
        }
    }
}
