﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HexGameUI : MonoBehaviour {

	public HexGrid grid;
    public BuildUnitMenu buildUnitMenu;
    public Player currentPlayer;
    public PlayerInfo playerInfo;

    HexCell currentCell;
    HexCell previousCell;

    bool unitIsAttacking;

	HexUnit selectedUnit;
    HexBuilding selectedBuilding;


    void Start() {
        currentPlayer = grid.playerList[0];
    }

    public void SetEditMode (bool toggle) {
		enabled = !toggle;
		grid.ShowUI(!toggle);
		grid.ClearPath();
	}

    public void EndTurn() {
        int nextPlayer = grid.playerList.IndexOf(currentPlayer) + 1;
        if (nextPlayer == grid.playerCount) { nextPlayer = 0; }
        currentPlayer = grid.playerList[nextPlayer];
        playerInfo.DisplayCurrentPlayer(nextPlayer+1);
        playerInfo.DisplayCurrentPlayerSupplyCount(currentPlayer.Supplies);
    }

	void Update () {
		if (!EventSystem.current.IsPointerOverGameObject()) {
			if (Input.GetMouseButtonDown(0) && !selectedUnit) {
				DoSelection();
			}
			else if (selectedUnit) {
                if (Input.GetMouseButtonDown(1)) {
                    grid.ClearPath();
                    grid.ClearSelection();
                    grid.ClearAttackSelection();
                    selectedUnit = null;
                    selectedBuilding = null;
                    unitIsAttacking = false;
                } else if (!unitIsAttacking) {
                    if (Input.GetMouseButtonDown(0)) {
                        DoMove();
                    } else {
                        DoPathfinding();
                    }
                } else if (Input.GetMouseButtonDown(0)) {
                    DoAttack();
                } else {
                    UpdateCurrentCell();
                }
			}
		}
	}

	void DoSelection () {
		grid.ClearPath();
        grid.ClearSelection();
        UpdateCurrentCell();
        if (currentCell) {
			selectedUnit = currentCell.Unit;
            selectedBuilding = currentCell.Building;
            if (selectedUnit){
                grid.FindDistancesTo(currentCell, selectedUnit.movementRange);
            } 
            else if (selectedBuilding)
            {
                buildUnitMenu.Open();
            }
		}
	}

	void DoPathfinding () {
		if (UpdateCurrentCell()) {
			if (currentCell && selectedUnit.IsValidDestination(currentCell)) {
                grid.FindPath(selectedUnit.Location, currentCell, selectedUnit.movementRange);
                grid.RefreshAttackables();
			}
			else {
				grid.ClearPath();
			}
		}
	}

	void DoMove () {
		if ((grid.HasPath && !currentCell.Unit) || currentCell.Unit == selectedUnit ) { //if there is a path to the selected cell, and if it is empty

            if (currentCell.Unit != selectedUnit) {
                Vector3 directionVector = (selectedUnit.Location.transform.position - currentCell.transform.position);
                selectedUnit.transform.rotation = Quaternion.LookRotation(directionVector);
                //selectedUnit.Orientation = selectedUnit.transform.rotation.y;
                selectedUnit.Location = currentCell;
            }

            grid.ClearPath();
            grid.ClearSelection();

            if (grid.FindAttackDistancesTo(currentCell, 1, 1)) {
                currentCell.EnableHighlight(Color.blue);
                unitIsAttacking = true;
            } else {
                selectedUnit = null;
            }
        }
	}

    void DoAttack()
    {
        if (grid.HasTargets && currentCell.isAttackSelected)
        {
            //currentCell.Unit.Die();
            //will that work? no it won't, have to go through the grid to remove from the list too.
            grid.RemoveUnit(currentCell.Unit);
        }
        grid.ClearPath();
        grid.ClearSelection();
        selectedUnit = null;
        grid.ClearAttackSelection();
        unitIsAttacking = false;
    }

    bool UpdateCurrentCell () {
		HexCell cell =
			grid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
		if (cell != currentCell) {
            previousCell = currentCell; //to be used to move & attack at once
			currentCell = cell;
			return true;
		}
		return false;
	}

    public void AddUnitToGrid(string unitType, HexCell buildingCell) {
        HexUnit unitToAdd = Array.Find<HexUnit>(currentPlayer.ps.hexUnits, t => t.name.Contains(unitType));

        grid.AddUnit(
            Instantiate(unitToAdd), buildingCell, UnityEngine.Random.Range(0f, 360f), currentPlayer.PlayerNumber
        );
        HexMapCamera.ValidatePosition();
    }
}