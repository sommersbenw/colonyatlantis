﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {

    public Text playerText;
    public Text suppliesText;

    public void DisplayCurrentPlayer (int currentPlayer) {
        playerText.text = "Player " + currentPlayer.ToString();
    }

    public void DisplayCurrentPlayerSupplyCount(int supplyCount) {
        suppliesText.text = supplyCount.ToString() + " Supplies";
    }

}
