﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class HexGrid : MonoBehaviour {

	public int cellCountX = 20, cellCountZ = 15;

	public HexCell cellPrefab;
	public Text cellLabelPrefab;
	public HexGridChunk chunkPrefab;
	public HexUnit[] unitPrefabs;
    public HexBuilding[] buildingPrefabs;

    public Texture2D noiseSource;

	public int seed;

    public bool HasPath {
		get {
			return currentPathExists;
		}
	}

    public bool HasTargets
    {
        get
        {
            return unitsToAttackExist;
        }
    }

    HexGridChunk[] chunks;
	HexCell[] cells;

	int chunkCountX, chunkCountZ;

	HexCellPriorityQueue searchFrontier;

	int searchFrontierPhase;

	HexCell currentPathFrom, currentPathTo;
    bool currentPathExists, selectionExists, isInRange, unitsToAttackExist;

	List<HexUnit> units = new List<HexUnit>();
    List<HexBuilding> buildings = new List<HexBuilding>();

    public List<Player> playerList = new List<Player>();
    public int playerCount = 2;
    public PlayerSettings player1, player2;

    void Awake () {

		HexMetrics.noiseSource = noiseSource;
		HexMetrics.InitializeHashGrid(seed);
        BuildPlayers();
        CreateMap(cellCountX, cellCountZ);
    }

    private void BuildPlayers() {
        for (int i = 0; i < playerCount; i++) {
            int playerNumber = i + 1;
            Player player = new Player(0, playerNumber, (playerNumber == 1) ? player1 : player2);
            playerList.Add(player);
        }
    }

    public void AddUnit (HexUnit unit, HexCell location, float orientation, int owner) {
		units.Add(unit);
		unit.transform.SetParent(transform, false);
		unit.Location = location;
		unit.Orientation = orientation;
        unit.ownerNumber = owner;
	}

	public void RemoveUnit (HexUnit unit) {
		units.Remove(unit);
		unit.Die();
	}

    public void AddBuilding(HexBuilding building, HexCell location, float orientation, int owner) {
        buildings.Add(building);
        building.transform.SetParent(transform, false);
        building.Location = location;
        building.Orientation = orientation;
        building.ownerNumber = owner;
    }

    public void RemoveBuilding(HexBuilding building) {
        buildings.Remove(building);
        building.Demolish();
    }

    public bool CreateMap (int x, int z) {
		if (
			x <= 0 || x % HexMetrics.chunkSizeX != 0 ||
			z <= 0 || z % HexMetrics.chunkSizeZ != 0
		) {
			Debug.LogError("Unsupported map size.");
			return false;
		}

		ClearPath();
        ClearSelection();
        ClearUnits();
        ClearBuildings();

        if (chunks != null) {
			for (int i = 0; i < chunks.Length; i++) {
				Destroy(chunks[i].gameObject);
			}
		}

		cellCountX = x;
		cellCountZ = z;
		chunkCountX = cellCountX / HexMetrics.chunkSizeX;
		chunkCountZ = cellCountZ / HexMetrics.chunkSizeZ;
		CreateChunks();
		CreateCells();
		return true;
	}

	void CreateChunks () {
		chunks = new HexGridChunk[chunkCountX * chunkCountZ];

		for (int z = 0, i = 0; z < chunkCountZ; z++) {
			for (int x = 0; x < chunkCountX; x++) {
				HexGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
				chunk.transform.SetParent(transform);
			}
		}
	}

	void CreateCells () {
		cells = new HexCell[cellCountZ * cellCountX];

		for (int z = 0, i = 0; z < cellCountZ; z++) {
			for (int x = 0; x < cellCountX; x++) {
				CreateCell(x, z, i++);
			}
		}
	}

	void ClearUnits () {
		for (int i = 0; i < units.Count; i++) {
			units[i].Die();
		}
		units.Clear();
	}

    void ClearBuildings() {
        for (int i = 0; i < buildings.Count; i++) {
            buildings[i].Demolish();
        }
        buildings.Clear();
    }

    void OnEnable () {
		if (!HexMetrics.noiseSource) {
			HexMetrics.noiseSource = noiseSource;
			HexMetrics.InitializeHashGrid(seed);
		}
	}

	public HexCell GetCell (Ray ray) {
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			return GetCell(hit.point);
		}
		return null;
	}

	public HexCell GetCell (Vector3 position) {
		position = transform.InverseTransformPoint(position);
		HexCoordinates coordinates = HexCoordinates.FromPosition(position);
		int index =
			coordinates.X + coordinates.Z * cellCountX + coordinates.Z / 2;
		return cells[index];
	}

	public HexCell GetCell (HexCoordinates coordinates) {
		int z = coordinates.Z;
		if (z < 0 || z >= cellCountZ) {
			return null;
		}
		int x = coordinates.X + z / 2;
		if (x < 0 || x >= cellCountX) {
			return null;
		}
		return cells[x + z * cellCountX];
	}

	public void ShowUI (bool visible) {
		for (int i = 0; i < chunks.Length; i++) {
			chunks[i].ShowUI(visible);
		}
	}

	void CreateCell (int x, int z, int i) {
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
		position.y = 0f;
		position.z = z * (HexMetrics.outerRadius * 1.5f);

		HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
		cell.transform.localPosition = position;
		cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);

		if (x > 0) {
			cell.SetNeighbor(HexDirection.W, cells[i - 1]);
		}
		if (z > 0) {
			if ((z & 1) == 0) {
				cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX]);
				if (x > 0) {
					cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX - 1]);
				}
			}
			else {
				cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX]);
				if (x < cellCountX - 1) {
					cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX + 1]);
				}
			}
		}

		Text label = Instantiate<Text>(cellLabelPrefab);
		label.rectTransform.anchoredPosition =
			new Vector2(position.x, position.z);
		cell.uiRect = label.rectTransform;

		cell.Elevation = 0;

		AddCellToChunk(x, z, cell);
	}

	void AddCellToChunk (int x, int z, HexCell cell) {
		int chunkX = x / HexMetrics.chunkSizeX;
		int chunkZ = z / HexMetrics.chunkSizeZ;
		HexGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

		int localX = x - chunkX * HexMetrics.chunkSizeX;
		int localZ = z - chunkZ * HexMetrics.chunkSizeZ;
		chunk.AddCell(localX + localZ * HexMetrics.chunkSizeX, cell);
	}

	public void Save (BinaryWriter writer) {
		writer.Write(cellCountX);
		writer.Write(cellCountZ);

		for (int i = 0; i < cells.Length; i++) {
			cells[i].Save(writer);
		}

		writer.Write(units.Count);
		for (int i = 0; i < units.Count; i++) {
			units[i].Save(writer);
		}

        writer.Write(buildings.Count);
        for (int i = 0; i < buildings.Count; i++) {
            buildings[i].Save(writer);
        }
    }

	public void Load (BinaryReader reader, int header) {
		ClearPath();
        ClearSelection();
        ClearUnits();
        ClearBuildings();

        int x = 20, z = 15;
		if (header >= 1) {
			x = reader.ReadInt32();
			z = reader.ReadInt32();
		}
		if (x != cellCountX || z != cellCountZ) {
			if (!CreateMap(x, z)) {
				return;
			}
		}

		for (int i = 0; i < cells.Length; i++) {
			cells[i].Load(reader);
		}
		for (int i = 0; i < chunks.Length; i++) {
			chunks[i].Refresh();
		}

		if (header >= 4) {
			int unitCount = reader.ReadInt32();
			for (int i = 0; i < unitCount; i++) {
				HexUnit.Load(reader, this);
			}

            int buildingCount = reader.ReadInt32();
            for (int i = 0; i < buildingCount; i++) {
                HexBuilding.Load(reader, this);
            }
        }
	}

	public void ClearPath () {
		if (currentPathExists) {
			HexCell current = currentPathTo;
			while (current != currentPathFrom) {
				current.SetLabel(null);
                DisablePathHighlight(current);
				current = current.PathFrom;
			}
            DisablePathHighlight(current);
            currentPathExists = false;
		}
		else if (currentPathFrom) {
            DisablePathHighlight(currentPathFrom);
            DisablePathHighlight(currentPathTo);
        }
		currentPathFrom = currentPathTo = null;
	}

    void ShowPath (int speed) {
        
        if (currentPathExists) {
            HexCell current = currentPathTo;
            while (current != currentPathFrom) {
                //int turn = current.Distance / speed;
                //current.SetLabel(turn.ToString());

                current.EnableHighlight(Color.yellow);
				current = current.PathFrom;
			}
            if (!currentPathTo.Unit) {
                currentPathTo.EnableHighlight(Color.yellow); //in path, no unit
            } else if (current != currentPathFrom) {

                currentPathTo.EnableHighlight(Color.red); // in path, with unit
            }
        } else {
            currentPathTo.EnableHighlight(Color.red); //off path
        }
        currentPathFrom.EnableHighlight(Color.blue); // origin
    }

	public void FindPath (HexCell fromCell, HexCell toCell, int speed) {
		ClearPath();
		currentPathFrom = fromCell;
		currentPathTo = toCell;
		currentPathExists = Search(fromCell, toCell, speed);
		ShowPath(speed);

	}

    public void RefreshAttackables() {
        for (int i = 0; i < cells.Length; i++) {
            if (cells[i].isAttackSelected) {
                cells[i].EnableHighlight(Color.red);
            }
        }
    }

    public void FindDistancesTo(HexCell fromCell, int speed)
    {
        if (selectionExists) { ClearSelection(); }
        selectionExists = true;
        for (int i = 0; i < cells.Length; i++)
        { 
            isInRange = Search(fromCell, cells[i], speed);
            if (cells[i].coordinates.DistanceTo(fromCell.coordinates)==0) {
                cells[i].EnableHighlight(Color.blue);
            }
            else if (isInRange && cells[i].Unit && cells[i].Unit.ownerNumber != fromCell.Unit.ownerNumber) {
                cells[i].isAttackSelected = true;
                cells[i].EnableHighlight(Color.red);
            }
            else if (isInRange && fromCell.Unit.IsValidDestination(cells[i]))
            {
                cells[i].isSelected = true;
                cells[i].EnableHighlight(Color.white);
            }
        }
    }

    public void DisablePathHighlight(HexCell fromCell) {
        if (fromCell.isSelected) {
            fromCell.EnableHighlight(Color.white);
        } else {
            fromCell.DisableHighlight();
        }
    }

    public void ClearSelection()
    {
        if (selectionExists)
        {
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i].isSelected = false;
                cells[i].DisableHighlight();
            }
            selectionExists = false;
        }
    }

    public void ClearAttackSelection() {
        if (unitsToAttackExist) {
            for (int i = 0; i < cells.Length; i++) {
                cells[i].isAttackSelected = false;
                cells[i].DisableHighlight();
            }
            unitsToAttackExist = false;
        }
    }

    bool Search (HexCell fromCell, HexCell toCell, int speed) {
		searchFrontierPhase += 2;
		if (searchFrontier == null) {
			searchFrontier = new HexCellPriorityQueue();
		}
		else {
			searchFrontier.Clear();
		}

		fromCell.SearchPhase = searchFrontierPhase;
		fromCell.Distance = 0;
		searchFrontier.Enqueue(fromCell);
        while (searchFrontier.Count > 0) {
			HexCell current = searchFrontier.Dequeue();
			current.SearchPhase += 1;

			if (current == toCell) {
				return true;
			}

			int currentTurn = current.Distance / speed;

			for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++) {
				HexCell neighbor = current.GetNeighbor(d);
				if (
					neighbor == null ||
					neighbor.SearchPhase > searchFrontierPhase
				) {
					continue;
				}
				if (neighbor.IsUnderwater) {
					continue; //we want to go through these in theory, right? 
				}
                if (neighbor.Unit) {
                    neighbor.EnableHighlight(Color.red);
                    neighbor.isAttackSelected = true;
                    continue;
                }

				HexEdgeType edgeType = current.GetEdgeType(neighbor);
				if (edgeType == HexEdgeType.Cliff) {
					continue;
				}
				int moveCost;
				if (current.HasRoadThroughEdge(d)) {
					moveCost = 1;
				}
                else if (current.IsUnderwater) {
                    moveCost = 999999999;  //replace with flying/sea calculations later
                } else if (current.Walled != neighbor.Walled) {
					continue;
				}
				else {
					moveCost = edgeType == HexEdgeType.Flat ? 5 : 10;
					moveCost += neighbor.UrbanLevel + neighbor.RockLevel +
						neighbor.PlantLevel + neighbor.TreeLevel;
				}

				int distance = current.Distance + moveCost;
				int turn = distance / speed;
				if (turn > currentTurn) {
                    //distance = turn * speed + moveCost;
                    continue;
                }

				if (neighbor.SearchPhase < searchFrontierPhase) {
                    neighbor.SearchPhase = searchFrontierPhase;
					neighbor.Distance = distance;
					neighbor.PathFrom = current;
					neighbor.SearchHeuristic =
						neighbor.coordinates.DistanceTo(toCell.coordinates);
					searchFrontier.Enqueue(neighbor);
				}
				else if (distance < neighbor.Distance) {
					int oldPriority = neighbor.SearchPriority;
					neighbor.Distance = distance;
					neighbor.PathFrom = current;
					searchFrontier.Change(neighbor, oldPriority);
				}
			}
		}
		return false;
	}

    public bool FindAttackDistancesTo(HexCell cell, int minAttack, int maxAttack) {
        unitsToAttackExist = false;
        
        for (int i = 0; i < cells.Length; i++) {
            cells[i].Distance =
                cell.coordinates.DistanceTo(cells[i].coordinates);
            if (cells[i].Distance <= maxAttack && cells[i].Distance >= minAttack && cells[i].Unit) {
                cells[i].EnableHighlight(Color.red); // in range, with unit
                //allow for attack action to be designated
                cells[i].isAttackSelected = true;
                unitsToAttackExist = true;
            }

        }
        return unitsToAttackExist;
    }

}