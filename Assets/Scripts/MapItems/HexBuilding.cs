﻿using UnityEngine;
using System.IO;
using System;

public class HexBuilding : MonoBehaviour {

    public static HexBuilding buildingPrefab;
    public int ownerNumber;

    public HexCell Location {
        get {
            return location;
        }
        set {
            if (location) {
                location.Building = null;
            }
            location = value;
            value.Building = this;
            transform.localPosition = value.Position;
        }
    }

    HexCell location;

    public float Orientation {
        get {
            return orientation;
        }
        set {
            orientation = value;
            transform.localRotation = Quaternion.Euler(0f, value, 0f);
        }
    }

    float orientation;

    public void ValidateLocation() {
        transform.localPosition = location.Position;
    }

    public void Demolish() {
        location.Unit = null;
        Destroy(gameObject);
    }

    public void Save(BinaryWriter writer) {
        location.coordinates.Save(writer);
        writer.Write(orientation);
        writer.Write(name.Substring(0, name.Length - 7));
        writer.Write(ownerNumber);
    }

    public static void Load(BinaryReader reader, HexGrid grid) {
        HexCoordinates coordinates = HexCoordinates.Load(reader);
        float orientation = reader.ReadSingle();
        string name = reader.ReadString();
        int ownerNumber = reader.ReadInt32();
        HexBuilding buildingToAdd = Array.Find<HexBuilding>(grid.playerList[(ownerNumber - 1)].ps.hexBuildings, t => t.name.Contains(name));
        grid.AddBuilding(
            Instantiate(buildingToAdd), grid.GetCell(coordinates), orientation, ownerNumber
        );
    }
}