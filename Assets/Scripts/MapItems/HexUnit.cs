﻿using System;
using System.IO;
using UnityEngine;

public class HexUnit : MonoBehaviour {

    public int movementRange;
    public int attackPower;
    public int ownerNumber;
    public int attackMinRange;
    public int attackMaxRange;
    public bool canAttackOnMove;
    public bool canCaptureBuilding;
 //   private float hitPoints_ = 10;
 //   private bool isActive_ = true;
    public bool isFlyingUnit;
    public Color myColor;
    public Color ownerColor;
//    private SpriteRenderer myHealth;
    public int cost_;

   /* public void SetUnitStartingData(int range, int power, int unitOwner, string type, int cost, bool canCapture, bool flyer = false, int attMin = 1, int attMax = 1, bool attackOnMove = true) {
        movementRange = range;
        attackPower = power;
        this.name = type;
        ownerNumber = unitOwner;
        myColor = GetComponent<SpriteRenderer>().color;
        isFlyingUnit = flyer;
        cost_ = cost;
        attackMinRange = attMin;
        attackMaxRange = attMax;
        canAttackOnMove = attackOnMove;
        return;
    }*/

    public HexCell Location {
		get {
			return location;
		}
		set {
			if (location) {
				location.Unit = null;
			}
			location = value;
			value.Unit = this;
			transform.localPosition = value.Position;
		}
	}

	HexCell location;

	public float Orientation {
		get {
			return orientation;
		}
		set {
			orientation = value;
			transform.localRotation = Quaternion.Euler(0f, value, 0f);
		}
	}

    float orientation;

	public void ValidateLocation () {
		transform.localPosition = location.Position;
	}

	public bool IsValidDestination (HexCell cell) {
        return !cell.IsUnderwater; //&& !cell.Unit; 
	}

	public void Die () {
		location.Unit = null;
		Destroy(gameObject);
	}

	public void Save (BinaryWriter writer) {
		location.coordinates.Save(writer);
		writer.Write(orientation);
        writer.Write(name.Substring(0, name.Length - 7));
        writer.Write(ownerNumber);
	}

	public static void Load (BinaryReader reader, HexGrid grid) {
		HexCoordinates coordinates = HexCoordinates.Load(reader);
		float orientation = reader.ReadSingle();
        string name = reader.ReadString();
        int ownerNumber = reader.ReadInt32();
        HexUnit unitToAdd = Array.Find<HexUnit>(grid.playerList[(ownerNumber-1)].ps.hexUnits, t => t.name.Contains(name));
        grid.AddUnit(
			Instantiate(unitToAdd), grid.GetCell(coordinates), orientation, ownerNumber
        );
        

    }
}